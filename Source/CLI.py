import asyncio

from Domain.GenerationRunners import GenerationRunners


async def main():
    print('CLI entry')

    await GenerationRunners.generate_squares_async()

asyncio.run(main())