from abc import ABC, abstractmethod
from PIL import Image

class IImageExporter(ABC):
    
    async def export_image_async(
        self,
        original_image: Image,
        image_name: str
    ) -> None:
        raise NotImplementedError('Must define')