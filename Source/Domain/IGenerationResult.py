
from abc import ABC, abstractmethod
from PIL import Image

class IGenerationResult(ABC):
    
    def get_image_name(
        self
    ) -> str:
        raise NotImplementedError('Must define')

    def get_image(
        self
    ) -> Image:
        raise NotImplementedError('Must define')