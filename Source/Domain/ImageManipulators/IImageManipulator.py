from abc import ABC, abstractmethod
from PIL import Image

class IImageManipulator(ABC):
    
    async def process_image_async(
        self,
        original_image: Image
    ) -> Image:
        raise NotImplementedError('Must define')