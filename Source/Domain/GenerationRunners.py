import uuid

from Domain.GenerationEngine import GenerationEngine
from Domain.ImageManipulators.DebugImageManipulator import DebugImageManipulator
from Domain.ImageManipulators.GenerativeSquaresImageManipulator import GenerativeSquaresImageManipulator
from Domain.Random import Random
from Infrastructure.ImageExporters.DebugImageExporter import DebugImageExporter
from Infrastructure.ImageExporters.FileImageExporter import FileImageExporter, FileImageTypes

from PIL import Image

class GenerationRunners:

    @staticmethod
    async def generate_squares_async() -> None:
        run_id = uuid.uuid1()
        image_manipulator = GenerativeSquaresImageManipulator(
            Random(run_id)
        )

        image_exporter = FileImageExporter(
            f'./output/{run_id}',
            FileImageTypes.PNG
        )

        generation_engine = GenerationEngine(
            image_manipulator,
            image_exporter
        )

        num_iterations: int = 10
        for i in range(num_iterations):
            image = Image.new('RGB', (2000, 2000))
            await generation_engine.generate_async(image, i)

    @staticmethod
    async def debug_generate_async() -> None:
        image_manipulator = DebugImageManipulator()
        image_exporter = DebugImageExporter()

        generation_engine = GenerationEngine(
            image_manipulator,
            image_exporter
        )

        await generation_engine.generate_async('debug')
