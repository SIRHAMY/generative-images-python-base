FROM continuumio/anaconda3

WORKDIR /home/app
USER $UID

COPY environment.yml environment.yml
RUN conda env create -f environment.yml

RUN /bin/bash -c "source activate generative-squares"

SHELL ["conda", "run", "-n", "generative-squares", "/bin/bash", "-c"]

RUN echo "Test that env is installed"
RUN python -c "import cv2"

COPY . .
RUN echo "starting operation"
ENTRYPOINT python ./Source/CLI.py