from Domain.IGenerationResult import IGenerationResult
from PIL import Image

class GenerationResult(IGenerationResult):

    def __init__(self, file_name: str, image: Image):
        self._file_name = file_name
        self._image = image

    def get_image_name(self) -> str:
        return self._file_name

    def get_image(self) -> Image:
        return self._image