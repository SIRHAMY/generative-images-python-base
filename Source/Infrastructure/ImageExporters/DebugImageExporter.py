from Domain.IImageExporter import IImageExporter
from PIL import Image

class DebugImageExporter(IImageExporter):
    def __init__(self):
        print('DebugImageExporter created')

    async def export_image_async(
        self,
        original_image: Image,
        image_name: str
    ) -> None:
        
        print('exporting image')