Everything you need to get started generating images in Python.

# Running

* Run: `sudo docker-compose up`
* Rebuild: `sudo docker-compose build --no-cache`

# Philosophy

This project is meant to make Python image generation simple and extensible. It differs from other image packages like p5py, PIL, and cv2 (some of which are utilized inside the project) as it hopes to make generation easier rather than provide base image manipulation capabilities.

* Add new APIs / Controllers in Source/
* Add new IImageExporters to persist images in different ways
* Add new IImageManipulators to add new drawing types
* Add new Utilities to add new drawing functionality