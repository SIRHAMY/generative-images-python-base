from Domain.ImageManipulators.IImageManipulator import IImageManipulator
from PIL import Image, ImageDraw
from Domain.Random import Random
from typing import Tuple

class GenerativeSquaresImageManipulator(IImageManipulator):

    def __init__(
        self,
        random: Random):
        print('Creating GenerativeSquares')
        self.random: Random = random

    async def process_image_async(
        self,
        original_image: Image
    ) -> Image:
        print('processing image')

        new_image = original_image.copy()
        width, height = new_image.size

        rectangle_width = 100
        rectangle_height = 100

        number_of_squares = self.random.getRandomInteger(10, 550)

        draw_image = ImageDraw.Draw(new_image)
        for i in range(number_of_squares):
            rectangle_x = self.random.getRandomInteger(0, width)
            rectangle_y = self.random.getRandomInteger(0, height)

            rectangle_shape = [
                (rectangle_x, rectangle_y),
                (rectangle_x + rectangle_width, rectangle_y + rectangle_height)]
            draw_image.rectangle(
                rectangle_shape,
                fill=(
                    self.random.getRandomInteger(0, 255),
                    self.random.getRandomInteger(0, 255),
                    self.random.getRandomInteger(0, 255)
                )
            )

        return new_image