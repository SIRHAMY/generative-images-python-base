from Domain.ImageManipulators import IImageManipulator
from Domain.GenerationResult import GenerationResult
from Domain.IGenerationResult import IGenerationResult
from Domain.IImageExporter import IImageExporter
from PIL import Image
from typing import Optional

class GenerationEngine:

    def __init__(self, 
        image_manipulator: IImageManipulator, 
        image_exporter: Optional[IImageExporter] = None):
        self.image_manipulator: IImageManipulator = image_manipulator
        self.image_exporter: Optional[IImageExporter] = image_exporter

    async def generate_async(self,
        image: Image,
        generation_name: str) -> IGenerationResult:
        print('generating')

        modified_image: Image = await self.image_manipulator.process_image_async(image)

        if self.image_exporter is not None:
            await self.image_exporter.export_image_async(
                modified_image,
                generation_name
            )

        return GenerationResult(
            generation_name,
            modified_image
        )