from Domain.ImageManipulators.IImageManipulator import IImageManipulator
from PIL import Image

class DebugImageManipulator(IImageManipulator):

    def __init__(self):
        print('Creating DebugImageManipulator')

    async def process_image_async(
        self,
        original_image: Image
    ) -> Image:
        print('processing image')
        return original_image

    # def GenerateSquares():
    #     # Call GenerateSquare multiple times...

    # def GenerateSquare():
