import random 

class Random:

    def __init__(self, seed: str):
        self.seed: str = seed
        random.seed(seed)

    def getRandomInteger(self, lowerBound: int, upperBound: int) -> int:
        return random.randint(lowerBound, upperBound)