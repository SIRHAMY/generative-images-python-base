import os

from Domain.IImageExporter import IImageExporter
from enum import Enum
from PIL import Image
from typing import Dict

class FileImageTypes(Enum):
    PNG = 1

class FileImageExporter(IImageExporter):
    def __init__(
        self,
        folder_path: str,
        file_image_type: FileImageTypes):
        self.folder_path: str = folder_path
        self.file_image_type: FileImageTypes = file_image_type

    async def export_image_async(
        self,
        original_image: Image,
        image_name: str
    ) -> None:
        print('exporting image')

        if not os.path.isdir(self.folder_path):
            os.mkdir(self.folder_path)
        
        save_file_path = FileImageExporter._construct_output_file_path(
            self.folder_path,
            image_name,
            self._get_file_ending_for_file_image_type(self.file_image_type)
        )
        original_image.save(save_file_path)
        print(f'image saved to: {save_file_path}')

    @staticmethod
    def _get_file_ending_for_file_image_type(image_type: FileImageTypes) -> str:
        file_image_types_to_file_ending: Dict[FileImageTypes, str] = {
            FileImageTypes.PNG: 'png'
        }
        return file_image_types_to_file_ending[image_type]

    @staticmethod
    def _construct_output_file_path(
        folder_path: str,
        image_name: str,
        file_ending: str
    ) -> str:
        return os.path.join(
            folder_path,
            f'{image_name}.{file_ending}'
        )
